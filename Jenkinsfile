def label = "mypod-${UUID.randomUUID().toString()}"
def serviceaccount = "jenkins-admin"
podTemplate(label: label, serviceAccount: serviceaccount, 
    containers: [containerTemplate(name: 'python', image: 'localhost:32121/root/docker_registry/python:3.8.2-alpine', ttyEnabled: true, command: 'cat'),
	containerTemplate(name: 'curl', image: 'localhost:32121/root/docker_registry/aiindevops.azurecr.io/curl:0.1', ttyEnabled: true, alwaysPullImage: true, command: 'cat'),
	containerTemplate(name: 'git-secrets', image: 'localhost:32121/root/docker_registry/aiindevops.azurecr.io/git-secrets:0.1', ttyEnabled: true, alwaysPullImage: true, command: 'cat'),
	containerTemplate(name: 'go', image: 'localhost:32121/root/docker_registry/golang:alpine3.9', ttyEnabled: true, command: 'cat'),
	containerTemplate(name: 'jq', image: 'stedolan/jq', ttyEnabled: true, command: 'cat'),
	containerTemplate(name: 'maven', image: 'maven:3.3.9-jdk-8-alpine', ttyEnabled: true, command: 'cat'),
	containerTemplate(name: 'kubeaudit', image: 'localhost:32121/root/docker_registry/aiindevops.azurecr.io/kube-audit:0.1', ttyEnabled: true, alwaysPullImage: true, command: 'cat'),
	containerTemplate(name: 'clair-scanner', image: 'localhost:32121/root/docker_registry/aiindevops.azurecr.io/clair-scanner:0.1', ttyEnabled: true, alwaysPullImage: true, command: 'cat',envVars: [
            envVar(key: 'DOCKER_SOCK', value: "$DOCKER_SOCK"),envVar(key: 'DOCKER_HOST', value: "$DOCKER_HOST")],
					volumes: [hostPathVolume(hostPath: "$DOCKER_SOCK", mountPath: "$DOCKER_SOCK")]),
    containerTemplate(name: 'helm', image: 'localhost:32121/root/docker_registry/lachlanevenson/k8s-helm:v2.9.1', command: 'cat', ttyEnabled: true),
        containerTemplate(name: 'kubectl', image: 'localhost:32121/root/docker_registry/aiindevops.azurecr.io/docker-kubectl:19.03-alpine', ttyEnabled: true, command: 'cat',
               volumes: [secretVolume(secretName: 'kube-config', mountPath: '/root/.kube')]),
    containerTemplate(name: 'docker', image: 'localhost:32121/root/docker_registry/docker:1.13', ttyEnabled: true, command: 'cat',envVars: [
            envVar(key: 'DOCKER_SOCK', value: "$DOCKER_SOCK"),envVar(key: 'DOCKER_HOST', value: "$DOCKER_HOST")])],
			volumes: [hostPathVolume(hostPath: "$DOCKER_SOCK", mountPath: "$DOCKER_SOCK")],
	imagePullSecrets: ['gcrcred']
    ) 
{

    node(label) {
            def GIT_URL= 'http://gitlab.ethan.svc.cluster.local:8084/gitlab/root/microservices_python_cartridge.git'
		def GIT_CREDENTIAL_ID ='gitlab'
		def GIT_BRANCH='master'
	
		/*** For Java - COMPONENT_KEY value should be same as what is given in pom file groupId:artifactID 
			 For NodeJs - COMPONENT_KEY value should be same as what is given as ProjectName in in sonar.properies file ***/		
		def COMPONENT_KEY='Python';
		def rootDir = pwd()	
		def SONAR_UI='http://sonar.ethan.svc.cluster.local:9001/sonar/api/measures/component?metricKeys=';
		
		/**** provide Sonar metrickeys which needs to be published to Jenkins console ***/
		//String metricKeys = "major_violations,minor_violations,critical_violations,blocker_violations,security_rating,complexity,violations,open_issues,test_success_density,test_errors,test_execution_time,security_remediation_effort,uncovered_conditions,classes,functions,line_coverage,sqale_rating,sqale_debt_ratio,reliability_remediation_effort,coverage,code_smells,bugs,vulnerabilities,sqale_index,tests,ncloc,quality_gate_details,duplicated_lines,cognitive_complexity";
		
		/*** Below variables used in the sonar maven configuration ***/
		def SONAR_SCANNER='org.sonarsource.scanner.maven'
		def SONAR_PLUGIN='sonar-maven-plugin:3.2'
		def SONAR_HOST_URL='http://sonar.ethan.svc.cluster.local:9001/sonar'
		
		/***  DOCKER_HUB_REPO_URL is the URL of docker hub ***/
		/***def DOCKER_HUB_REPO_URL='aiindevops.azurecr.io'
		def DOCKER_HUB_REPO_NAME='aiindevops.azurecr.io'
		def DOCKER_HUB_ACCOUNT = 'aiindevops.azurecr.io'***/
		//def DOCKER_CREDENTIAL_ID='gitlab'
        def DOCKER_IMAGE_NAME = 'python'
        def IMAGE_TAG = '0.0.0.8'
		def GCR_HUB_ACCOUNT = 'localhost:32121'
        def GCR_HUB_ACCOUNT_NAME = 'root'
        def GCR_HUB_REPO_NAME='docker_registry'
		/*** Kuberenetes  ***/
		def K8S_DEPLOYMENT_NAME = 'python'
		def K8S_DEPLOYMENT = 'mongopy'
		
		/*** Application ELB ***/
		def LB
			         
     try {
	 
          stage('Git Checkout') {
            git branch: GIT_BRANCH, url: GIT_URL,credentialsId: GIT_CREDENTIAL_ID
			def function = load "${WORKSPACE}/JenkinsFunctions_Python.groovy"
			def Nap = load "${WORKSPACE}/git_scan_nonallowed.groovy"
			def Ap = load "${WORKSPACE}/git_scan_allowed.groovy"
			
			// Below two lines are to publish last commited user name and email into jenkins console logs 
            sh 'GIT_NAME=$(git --no-pager show -s --format=\'%an\' $GIT_COMMIT)'
            sh 'GIT_EMAIL=$(git --no-pager show -s --format=\'%ae\' $GIT_COMMIT)'
			
         stage('Git Secret') {
		    container('git-secrets') {
            Nap.nonAllowedPattern()
            Ap.AllowedPattern()	
		    sh 'git secrets --scan'
        
          }
		}
           
	        stage('Build  Project') {
                container('python') {
                  function.buildMethod()
				  //sh 'nosetests -v'
                }
            }
            stage('MongoDB Instance Creation')
            {
              container('kubectl') {
                try{
                  sh("kubectl get deployment/mongopy -n ethan")
                  if(true){
                sh ("kubectl set image deployment/${K8S_DEPLOYMENT} ${K8S_DEPLOYMENT}=${GCR_HUB_ACCOUNT}/${GCR_HUB_ACCOUNT_NAME}/${GCR_HUB_REPO_NAME}/mongo:3.4 -n ethan")
                  }
                }catch(e){
              sh("kubectl apply -f mongo.yaml -n ethan")
              echo "deploying"
              }
			  sh ("kubectl get pods | grep mongopy")
              sh ("kubectl get svc mongopy -n ethan")
                
              }
            }
	        stage('Unit Test') {
                    container('python') {
                    function.testMethod()
                        
                   }
             }
               
                
			 stage('SonarQube Analysis') {
			withCredentials([usernamePassword(credentialsId: 'SONAR', passwordVariable: 'PASSWORD', usernameVariable: 'USERNAME')]){
			//withSonarQubeEnv('SonarQube') {
			container('maven') {
			sh ("curl -u ${USERNAME}:${PASSWORD} -k http://sonar.ethan.svc.cluster.local:9001/sonar/api/qualitygates/show?name=python_QG > qualitygate.json")
			sh 'cat qualitygate.json'
			}
			container('jq') {
			sh '''
			export SONAR_PROJECT_KEY='hello-world-python'
			export SONAR_QG_NAME='python_QG'
			apt update
			apt -y install curl
			echo $SONAR_PROJECT_NAME
			echo $SONAR_PROJECT_KEY
			echo $SONAR_QG_NAME
			echo "Creating sonar gateway"
			curl -u ${USERNAME}:${PASSWORD} -k -X POST "http://sonar.ethan.svc.cluster.local:9001/sonar/api/qualitygates/create?name=$SONAR_QG_NAME"
			curl -u ${USERNAME}:${PASSWORD} -k "http://sonar.ethan.svc.cluster.local:9001/sonar/api/qualitygates/show?name=$SONAR_QG_NAME" > qualitygate.json
		    QG_ID=$(cat qualitygate.json | jq -r ".id")
		    curl -u ${USERNAME}:${PASSWORD} -k -X POST "http://sonar.ethan.svc.cluster.local:9001/sonar/api/qualitygates/create_condition?gateId=$QG_ID&metric=coverage&op=LT&error=80"
			curl -u ${USERNAME}:${PASSWORD} -k -X POST "http://sonar.ethan.svc.cluster.local:9001/sonar/api/qualitygates/create_condition?gateId=$QG_ID&metric=duplicated_lines_density&op=GT&error=10"
			export SONAR_PROJECT_NAME='hello-world-python'
			curl -u ${USERNAME}:${PASSWORD} -k -X POST "http://sonar.ethan.svc.cluster.local:9001/sonar/api/projects/create?project=$SONAR_PROJECT_KEY&name=$SONAR_PROJECT_NAME"
			curl -u ${USERNAME}:${PASSWORD} -k "http://sonar.ethan.svc.cluster.local:9001/sonar/api/projects/search?" > project.json
			ls -lrta
			cat qualitygate.json
			cat project.json
			PROJECT_ID=$(cat project.json | jq -r \'.components[] | select(.name=="'$SONAR_PROJECT_NAME'") | .id\')
			echo $QG_ID
			echo $PROJECT_ID
			curl -u ${USERNAME}:${PASSWORD} -k -X POST "http://sonar.ethan.svc.cluster.local:9001/sonar/api/qualitygates/select?gateId=$QG_ID&projectKey=$SONAR_PROJECT_KEY"
			curl -u ${USERNAME}:${PASSWORD} -k "http://sonar.ethan.svc.cluster.local:9001/sonar/api/qualitygates/show?name=$SONAR_QG_NAME" > qualitygate.json
			echo "Updating sonar gateway"
			cat qualitygate.json | jq -r ".conditions[].id" > cgid.txt
            cgid1=$(head -n 1 cgid.txt)
            echo $cgid1
            curl -u ${USERNAME}:${PASSWORD} -k -X POST "http://sonar.ethan.svc.cluster.local:9001/sonar/api/qualitygates/update_condition?gateId=$QG_ID&id=$cgid1&metric=coverage&op=LT&error=0"
            cgid2=$(sed -n '2p' cgid.txt)
            echo $cgid2
            curl -u ${USERNAME}:${PASSWORD} -k -X POST "http://sonar.ethan.svc.cluster.local:9001/sonar/api/qualitygates/update_condition?gateId=$QG_ID&id=$cgid2&metric=duplicated_lines_density&op=GT&error=80"
			'''
          }
			
			
			    withSonarQubeEnv('SonarQube') {
                println('Sonar Method enter');
				function.sonarMethod()  
                echo "Access the SonarQube URL from the Platform Dashboard tile"
                }
                sh 'sleep 30'				
                }
			   
			   timeout(time: 1, unit: 'HOURS') {
                                def qg = waitForQualityGate()
                                if (qg.status != 'OK') {
                                    error "Pipeline aborted due to quality gate failure: ${qg.status}"
                                }
                            }
             
             } 
			 
            /* Below stage is to publish tools logs to Jenkins console */
          /*stage('Publish SonarQube Report') {
                  container ('curl') {
					String[] metricKeyList = metricKeys.split(",");
					for(String key : metricKeyList){
					String var1=SONAR_UI+key+'&componentKey='+COMPONENT_KEY;
                    sh('curl -k -u '+'"'+var1+'"');  
                  } 
			    }
                    
            } */
			
		    stage ('Create Docker Image'){
			container('docker'){
			sh ("docker build -t ${GCR_HUB_ACCOUNT}/${GCR_HUB_ACCOUNT_NAME}/${GCR_HUB_REPO_NAME}/${DOCKER_IMAGE_NAME}:${IMAGE_TAG} --network=host .")
				}
			}  
	         
			stage('Create Jenkinslave Service') {
            container('kubectl') {
            echo 'Deploying....'
            
            sh """
			echo ${label}
			cat ${WORKSPACE}/clair-scanner.yaml | sed "s/{{parm}}/${label}/g" | kubectl apply -f -
			"""
			
			
			}
        }

            stage ('Docker Image Scan') {
		      container('clair-scanner') {		
		     // sh ("clair-scanner -w 'mywhitelist.yaml' -c 'http://clair:6060' --ip='${label}' -t 'High' ${GCR_HUB_ACCOUNT}/${GCR_HUB_ACCOUNT_NAME}/${GCR_HUB_REPO_NAME}/${DOCKER_IMAGE_NAME}:${IMAGE_TAG}")
                sh ("clair-scanner -w 'mywhitelist.yaml' -c 'http://clair:6060' --ip=`hostname -i` -t 'High' ${GCR_HUB_ACCOUNT}/${GCR_HUB_ACCOUNT_NAME}/${GCR_HUB_REPO_NAME}/${DOCKER_IMAGE_NAME}:${IMAGE_TAG}")
		      		
		       }
            }
			   
	        stage('Publish Docker Image') {
            container('docker') {
              withCredentials([[$class: 'UsernamePasswordMultiBinding',
                credentialsId: 'gitlab',
                usernameVariable: 'DOCKER_HUB_USER',
                passwordVariable: 'DOCKER_HUB_PASSWORD']]) {
                    sh ('docker login -u ${DOCKER_HUB_USER} -p ${DOCKER_HUB_PASSWORD} '+GCR_HUB_ACCOUNT)
                    sh ("docker push ${GCR_HUB_ACCOUNT}/${GCR_HUB_ACCOUNT_NAME}/${GCR_HUB_REPO_NAME}/${DOCKER_IMAGE_NAME}:${IMAGE_TAG}")
                }
                
            }
			}
			
			stage('Delete Jenkinslave Service') {
            container('kubectl') {
            sh """
                kubectl delete svc '${label}'
            """
            }
        }
            	stage ('Kubectl Check')
		   {
			   container('kubectl')
			   {
		   //sh 'kubectl get node  >> nodes.txt'
		    //sh 'kubectl auth can-i list nodes'
		  
		   sh 'kubectl get nodes  >> nodes.txt'
		    sh label: '', script: '''
		    if grep -q master nodes.txt; then
			echo "master found" >> master.txt
			else
			echo not found
		    fi
		   echo cat master.txt
		   '''
			   }
		   
		  if (fileExists('master.txt'))
		  {
		
			
			echo " It is  non managed kubernetes service hence executing kube audit and kube bench stages"
			stage('Kube-Bench Scan') { 
		    container('kubectl') {		
		    sh '''
            v=`kubectl version --short | grep -w 'Server Version'`
			v1=`echo $v | awk -F: '{print $2}'`
			v2=`echo $v1 | sed 's/v//g'`
			versionserver=`echo $v2 | cut -f1,2 -d'.'`
			echo kubernetes version is ${versionserver}
		    kubectl run --rm -i kube-bench-master-python-${BUILD_ID} --image=aquasec/kube-bench:latest --restart=Never --overrides="{ \\"apiVersion\\": \\"v1\\", \\"spec\\": { \\"hostPID\\": true, \\"nodeSelector\\": { \\"kubernetes.io/role\\": \\"master\\" }, \\"tolerations\\": [ { \\"key\\": \\"node-role.kubernetes.io/master\\", \\"operator\\": \\"Exists\\", \\"effect\\": \\"NoSchedule\\" } ] } }" -- master --version ${versionserver}
		    kubectl run --rm -i kube-bench-node-python-${BUILD_ID} --image=aquasec/kube-bench:latest --restart=Never --overrides="{ \\"apiVersion\\": \\"v1\\", \\"spec\\": { \\"hostPID\\": true } }" -- node --version ${versionserver}
		    '''
			}       
			}
		  
            
            stage('Kubeaudit Scan') {
		          container('kubeaudit') {		
		          sh 'kubeaudit -a allowpe'
		        }
            }
		}
		else
		{
			echo " It is Managed Kubernetes service hence executing kubebench and kube-audit only on nodes "
			stage('Kube-Bench Scan') { 
		    container('kubectl') {	
			
			sh '''
            v=`kubectl version --short | grep -w 'Server Version'`
			v1=`echo $v | awk -F: '{print $2}'`
			v2=`echo $v1 | sed 's/v//g'`
			versionserver=`echo $v2 | cut -f1,2 -d'.'`
			echo kubernetes version is ${versionserver}
			kubectl run --rm -i kube-bench-node-python-${BUILD_ID} --image=aquasec/kube-bench:latest --restart=Never --overrides="{ \\"apiVersion\\": \\"v1\\", \\"spec\\": { \\"hostPID\\": true } }" -- node --version ${versionserver}
			'''
			}
			}
			  stage('Kubeaudit Scan') {
		          container('kubeaudit') {		
		          sh 'kubeaudit -a allowpe'
		        }
		   }
		   }
		   }
			
			
            stage('Deploy Build to Kubernetes') {
              sh "sed -i 's/imagetag/${IMAGE_TAG}/g' python-app.yaml"
            container('kubectl') {
             try{ 
             //  sh("kubectl delete deployment/${K8S_DEPLOYMENT_NAME} -n ethan")
              sh("kubectl get deployment/${K8S_DEPLOYMENT_NAME} -n ethan")
              if(true){
                sh ("kubectl set image deployment/${K8S_DEPLOYMENT_NAME} ${K8S_DEPLOYMENT_NAME}=${GCR_HUB_ACCOUNT}/${GCR_HUB_ACCOUNT_NAME}/${GCR_HUB_REPO_NAME}/${DOCKER_IMAGE_NAME}:${IMAGE_TAG} -n ethan") 
              }
              } 
              catch(e){
              sh("kubectl apply -f python-app.yaml -n ethan")
              echo "deploying"
              }
              sh ("kubectl get pods | grep python")
              sh ("kubectl rollout status deployment/${K8S_DEPLOYMENT_NAME} -n ethan")
			  sh ("kubectl get svc python-service -n ethan")
              LB = sh (returnStdout: true, script: '''kubectl get svc python-service -n ethan -o jsonpath="{ .status.loadBalancer.ingress[*]['ip', 'hostname']}" ''')
              echo "LB: ${LB}"
              def loadbalancer = "http://"+LB
              echo "loadbalancer: ${loadbalancer}"
              sleep 40 // seconds           
				}
			} 
			stage('OWASP Webscan') {
                    container('docker') {
                       try{
					sh ("docker run -t owasp/zap2docker-stable zap-baseline.py -t 'http://${LB}' ")
                   }catch(Exception e){
                       sh 'echo "error"'
                   }
                    sh 'echo "Check"'
                   }
               }
			
	}

        currentBuild.result = 'SUCCESS'
        echo "RESULT: ${currentBuild.result}"
        echo "Finished: ${currentBuild.result}"
              
        } catch (Exception err) {
        currentBuild.result = 'FAILURE'
        echo "RESULT: ${currentBuild.result}"
        echo "Finished: ${currentBuild.result}"
               }               
     logstashSend failBuild: false, maxLines: -1
     }
	 
  }

