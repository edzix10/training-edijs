import os
import sqlite3
from flask import Flask, redirect, url_for, request, render_template
from pymongo import MongoClient

app = Flask(__name__)

client = MongoClient(
    "mongodb://mongopy:27017/fladocks")
db = client['fladocks']
#from flask_sqlalchemy import SQLAlchemy

#app = Flask(__name__)
#db = sqlite3.connect('./todo.db')

@app.route('/')
def todo():
    _items = db.todos.find()
    items = [item for item in _items]

    return render_template('index.html', items=items)


@app.route('/new', methods=['POST'])
def new():

    item_doc = {
        'name': request.form['name'],
        'description': request.form['description']
    }
    db.todos.insert_one(item_doc)

    return redirect(url_for('todo'))


if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
