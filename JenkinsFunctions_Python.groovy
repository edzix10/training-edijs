// Below methods are used to build and test the python code with pip
def buildMethod() {
    println('pythonBuildMethod enter');
    //sh('mvn clean install package');
	sh ('pip install -r requirements.txt');
    println('pythonBuildMethod exit');
}

def testMethod() {
    println('pythonTestMethod enter');
    //sh('mvn test');
        sh ('cp app.py tests');
//	sh ('pytest -v');
        sh ('python -m unittest -v tests/test_endpoints.py');
    println('pythonTestMethod exit');
}

def sonarMethod() {
	println('Sonar Method enter');
    def scannerHome = tool 'Sonar Scanner';
	sh "${scannerHome}/bin/sonar-scanner -Dsonar.login=$USERNAME -Dsonar.password=$PASSWORD";
    println('Sonar Method exit');
	
}

return this // Its important to return after all the functions.
